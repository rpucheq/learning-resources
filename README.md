# Main
* [File Format Specifications](https://github.com/samtools/hts-specs)
* UCSC [Training](https://genome.ucsc.edu/training/index.html) & [FAQ](https://genome.ucsc.edu/FAQ/)
* BioInfoBook [Figures](http://bioinfbook.org/php/powerpoints)

----------

## Books
* [Molecular Biology of the Cell](https://www.ncbi.nlm.nih.gov/books/NBK21054/)
* [An Introduction to Genetic Analysis](https://www.ncbi.nlm.nih.gov/books/NBK21766/)


* [The Linux CLI](http://linuxcommand.org/tlcl.php)
* [Unix Grymoire](http://www.grymoire.com/)
* [Algorithms and Data Structures](http://interactivepython.org/runestone/static/pythonds/index.html)

## Programming Languages
* [Julia](http://julialang.org/learning/) ([book](https://en.wikibooks.org/wiki/Introducing_Julia))
* [Python](http://scipy.org/topical-software.html)
  * [Interactive Py](http://interactivepython.org/runestone/static/pythonds/index.html)
  * [Py For Biologists](http://pythonforbiologists.com/index.php/introduction-to-python-for-biologists/)
  * [Matplotlib](http://www.loria.fr/~rougier/teaching/matplotlib/)
  * [Data Analysis](http://biobits.org/pydata.html)
  * [Machine Learning](http://scikit-learn.org/stable/documentation.html)
* [Haskell](http://learnyouahaskell.com/chapters)
* R
  * [Quick-R](http://www.statmethods.net/)
  * [Swirl](http://swirlstats.com/students.html)
  * [R & NGS](http://manuals.bioinformatics.ucr.edu/home/ht-seq#TOC-SOAP)
  * [Bioconductor Lib](http://bioconductor.org/help/workflows/)
  * [RSeek Lib Search](http://www.rseek.org/)
* Perl
  * [Perl 6](http://perl6intro.com/)
  * [Unix and Perl for Biologists](http://korflab.ucdavis.edu/Unix_and_Perl/index.html)
* Other stuff:
  * LaTeX
  * Markdown
  * RegEx [Generator](http://www.regexr.com/)

*IDK why I made this list. See [this one](http://hackr.io/).*

## Online courses and Tutorials
* [OpenHelix Tutorials](http://www.openhelix.com/freeTutorials.cgi)
* [BioStars Tutorials](https://www.biostars.org/t/Tutorials/)
* [GenomeSpace Recipes](http://recipes.genomespace.org/home)


  Try searching keywords like bioinformatics, biology, genomic, genetic, data analysis, data science, computer science, life science... in these websites:
- [Coursera](https://www.coursera.org/)
- [edX](https://www.edx.org/)
- [MIT](http://ocw.mit.edu/courses)
- [eBiomics](http://ebiomics.sdcinfo.com/)

## Software Documentation
* [GATK](https://www.broadinstitute.org/gatk/guide/best-practices.php)
* samtools 1:[BioBits'](http://biobits.org/samtools_primer.html) 2:[DaveTang's](https://github.com/davetang/learning_bam_file) 3:[YanhuiFan's](https://felixfan.github.io/bam-sam/) 4:[BinaryFlags](https://broadinstitute.github.io/picard/explain-flags.html)
* [bcftools](https://samtools.github.io/bcftools/howtos/variant-calling.html) 1:[GATK](http://gatkforums.broadinstitute.org/gatk/discussion/1268/what-is-a-vcf-and-how-should-i-interpret-it) 2:[DaveTang's](https://github.com/davetang/learning_vcf_file) 3:[Paper](https://www.researchgate.net/publication/230658044_A_beginners_guide_to_SNP_calling_from_high-Throughput_DNA-sequencing_data)
* bedtools 1:[Quinlan](http://quinlanlab.org/tutorials/bedtools/bedtools.html) 2:[YanhuiFan's](https://felixfan.github.io/bedtools/)
* [bedops](https://bedops.readthedocs.org/en/latest/content/usage-examples.html)
* BWA
* bowtie2
* [picard](http://broadinstitute.github.io/picard/)
* [jvarkit](https://github.com/lindenb/jvarkit)
* [GATK Tool Index](https://www.broadinstitute.org/gatk/guide/tooldocs/index)
* Visualization
  * [Circos](http://circos.ca/documentation/course/)
* Quality control
  * [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
* Variant detection/calling
 * [Exomiser](http://www.sanger.ac.uk/science/tools/exomiser) disease causing
 * [PopSV](https://github.com/jmonlong/PopSV) structural variants
 * [Silva](http://compbio.cs.toronto.edu/silva/) silent mutations
* [Variant Effect Predictor](http://www.ensembl.org/info/docs/tools/vep/index.html)
* Annotation
  * [ANNOVAR](http://annovar.openbioinformatics.org/en/latest/)
  * [BioGPS](http://biogps.org/help/)
* Graphical User Interface (GUI) apps
  * [Integrative Genomics Viewer (IGV)](https://www.broadinstitute.org/software/igv/)
  * [NCBI Genome Workbench](https://www.ncbi.nlm.nih.gov/tools/gbench/)
  * [Savant](http://genomesavant.com/p/savant/index)
  * [JBrowse](http://jbrowse.org/)

*IDK why I made this list. See [this one](https://bio-tools.org/)*.

## Other non-specific software
REMEMBER: If you need 'something' done, there might be a tool/plugin/addon just for that. Always 'ask' your search engine of preference!
* Text editor or IDE
  - [Atom](http://atom.io/)
  - Eclipse
  - Vim
  - Emacs
* Reference Manager
  - Mendeley
  - Zothero
  - Colwiz
  - ReadCube
  - [any other](https://en.wikipedia.org/wiki/Comparison_of_reference_management_software)
* Version control
  - [Git](https://git-scm.com/)
     - [HowTo Guide](https://githowto.com/)
     - [Try Online](https://try.github.io/)
     - [Pro Book](https://progit.org/)
     - [Short Videos](https://git-scm.com/videos)
  - Mercurial
  - SVN
* Web Browser Addons
  - uBlock Origin: remove ads.
  - Greasemonkey: tweak and/or interact with websites.
* Cloud storage: ENCRYPT YOUR DATA!
  - Cryfs
  - VeraCrypt

## Puzzles
  - [Code Golf](https://codegolf.stackexchange.com/)
  - [Project Euler](https://projecteuler.net/)
  - [Rosalind](http://rosalind.info/)

## Forums Q&A
* [BioStars](https://www.biostars.org/)
* [SEQAnswers](http://seqanswers.com/)
* [Reddit](https://www.reddit.com/r/bioinformatics/)
* [Biology Q&A](https://biology.stackexchange.com/)
* [Unix and GNU/Linux Q&A](https://unix.stackexchange.com/)
* [Code Q&A](http://stackoverflow.com/)

## Wikis
* [SEQAnswers](http://seqanswers.com/wiki/SEQanswers)
* [ArchWiki](https://wiki.archlinux.org/)
* [ZSH](http://zshwiki.org/home/examples/zleiab) or BASH

## RSS to follow **TODO**
*I've shared my [feedly.opml](feedly.opml) in this repository.*

## Cheatsheets **TODO**
*See [here](http://overapi.com/) meanwhile.*

## Illegal resources you should avoid
* Sci-Hub
* LibGen
* Bookfi

## Enjoy
[Open Culture](http://www.openculture.com/)
